import { Config } from "./internals/Config";
import { EventEmitter } from "events";
import { IConnector } from "./interfaces/IConnector";
export declare class HomeLight extends EventEmitter {
    private _configuration;
    constructor(configuration: Config);
    searchDevices(connectorName?: string): void;
    getConnector(connectorName: string): IConnector | void;
    private _onDeviceListUpdated;
}
