export interface IColor {
    toHSV(): ColorFormats.HSV;
    toRGB(): ColorFormats.RGB;
    toBGR(): ColorFormats.RGB;
    toHSL(): ColorFormats.HSL;
}
