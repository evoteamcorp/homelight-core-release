export interface IConnector {
    getName(): string;
    searchDevices(): void;
    on(eventName: string, handler: any): void;
    setNoLatencyMode(state: boolean): void;
}
