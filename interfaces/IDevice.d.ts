import { IColor } from "./IColor";
export interface IDevice {
    type: string;
    ip: string;
    macAddress: string;
    on(zone?: number): void;
    off(zone?: number): void;
    setBrightness(percent: number, zone?: number | Array<number>): void;
    setColor(color: IColor, zone?: number | Array<number>): void;
    setWhite(zone?: number | Array<number>, brightness?: number): void;
    setNightMode(zone?: number | Array<number>): void;
}
