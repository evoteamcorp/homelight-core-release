import { IColor } from "../interfaces/IColor";
export declare class Color implements IColor {
    private _hue;
    private _saturation;
    private _value;
    static fromString(color: string): Color;
    constructor(h: number, s: number, v: number);
    toHSV(): ColorFormats.HSV;
    toRGB(): ColorFormats.RGB;
    toBGR(): ColorFormats.RGB;
    toHSL(): ColorFormats.HSL;
}
