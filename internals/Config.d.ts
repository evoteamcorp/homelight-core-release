import { IConnector } from "../interfaces/IConnector";
export declare class Config {
    private _connectors;
    constructor(connectors?: Array<IConnector>);
    getConnectors(): Array<IConnector>;
    getConnector(name: string): IConnector | void;
}
